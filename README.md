# Flow Grant Type Authorization Code #

Cek dulu halaman konfigurasi OpenID dengan mengirim `GET` request ke [http://localhost:8080/.well-known/openid-configuration](http://localhost:8080/.well-known/openid-configuration). 

Hasilnya adalah sebagai berikut

```json
{
  "issuer": "http://localhost:8080",
  "authorization_endpoint": "http://localhost:8080/oauth2/authorize",
  "device_authorization_endpoint": "http://localhost:8080/oauth2/device_authorization",
  "token_endpoint": "http://localhost:8080/oauth2/token",
  "token_endpoint_auth_methods_supported": [
    "client_secret_basic",
    "client_secret_post",
    "client_secret_jwt",
    "private_key_jwt"
  ],
  "jwks_uri": "http://localhost:8080/oauth2/jwks",
  "userinfo_endpoint": "http://localhost:8080/userinfo",
  "end_session_endpoint": "http://localhost:8080/connect/logout",
  "response_types_supported": [
    "code"
  ],
  "grant_types_supported": [
    "authorization_code",
    "client_credentials",
    "refresh_token",
    "urn:ietf:params:oauth:grant-type:device_code"
  ],
  "revocation_endpoint": "http://localhost:8080/oauth2/revoke",
  "revocation_endpoint_auth_methods_supported": [
    "client_secret_basic",
    "client_secret_post",
    "client_secret_jwt",
    "private_key_jwt"
  ],
  "introspection_endpoint": "http://localhost:8080/oauth2/introspect",
  "introspection_endpoint_auth_methods_supported": [
    "client_secret_basic",
    "client_secret_post",
    "client_secret_jwt",
    "private_key_jwt"
  ],
  "subject_types_supported": [
    "public"
  ],
  "id_token_signing_alg_values_supported": [
    "RS256"
  ],
  "scopes_supported": [
    "openid"
  ]
}
```

Kita ingin mendapatkan url `authorization_endpoint`, yaitu `http://localhost:8080/oauth2/authorize`

Cara mendapatkan `access_token` :

1. Buka url authorization dengan browser. 

    [http://localhost:8080/oauth2/authorize?client_id=app01&response_type=code&redirect_uri=https://example.com/oauth-test&scope=openid&state=1234](http://localhost:8080/oauth2/authorize?client_id=app01&response_type=code&redirect_uri=https://example.com/oauth-test&scope=openid&state=1234)

    Kita akan diminta login. Masukkan username `user001` dan password `abcd`. 

2. Setelah login, kita akan di-redirect sesuai dengan `redirect_uri`. Di address bar browser, kita akan mendapati alamat berikut : `https://example.com/oauth-test?code=ZKAXMZXsHgn_Uj7bG2N6NTrExZd5-8cV0F7E6Ezn_3n2KC2s4KnX5CsSz2JhsLe7uBO1gwa3frH5hAFfjCOb97dltkr9Xoh6uOaIsjwqSQEHb5aW52x61mnqC3ucYxGx&state=1234`

    ![[Redirect setelah login](img/redirect-setelah-login.png)](img/redirect-setelah-login.png)

3. Di alamat redirect, ada authorization code, yaitu `ZKAXMZXsHgn_Uj7bG2N6NTrExZd5-8cV0F7E6Ezn_3n2KC2s4KnX5CsSz2JhsLe7uBO1gwa3frH5hAFfjCOb97dltkr9Xoh6uOaIsjwqSQEHb5aW52x61mnqC3ucYxGx`. Tukarkan kode ini menjadi `access_token`

4. Lakukan http request `POST` ke `token endpoint`, yaitu `http://localhost:8080/oauth2/token`. Parameter dikirim dengan format `application/x-www-form-urlencoded` :

    * client_id : `app01`
    * redirect_uri : `https://example.com/oauth-test`
    * grant_type : `authorization_code`
    * code : `p7W0Y-KX2WDbRGJcFbSCuV0UXubaHxrS6n1McmmtvqAdfq9PsCETjnPjFumqR76AHO-jBluJmtLk2GiU9jLst9ibzTIZPNTrDB-o5IUe6q-qfACWGcJ3j36QIChMQ0Qz`

    ![[Request Param](img/request-param.png)](img/request-param.png)

    Dengan menggunakan Basic Authentication dengan:
    
    * Username : client id
    * Passsword : client secret

    ![[Basic Auth](img/basic-auth.png)](img/basic-auth.png)

5. Kita akan mendapatkan response sebagai berikut

    ```json
    {
        "access_token": "eyJraWQiOiI2YjdjN2UxYi0wYzdiLTQ2ODAtYWQ4OS1kZThkYzVkMGY5MDEiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ1c2VyMDAxIiwiYXVkIjoiYXBwMDEiLCJuYmYiOjE2OTUwMTkzMzYsInNjb3BlIjpbIm9wZW5pZCJdLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAiLCJleHAiOjE2OTUwMTk2MzYsImlhdCI6MTY5NTAxOTMzNn0.NxhubduWmca2dVod5Kj0sO1auYtaj7ysaFHRrm49NPvAoNlTGHrxlsx9ANKgvinltaJGqodc9FnJSW6HBWlfDh-QdFAAKk7tmCbs9wugRVqJwQpp9vIbnKK46u0lzGiXUh2NgUenKSQjlbtRgW58-BC7U05UCakK64SXAaEH8ZoUlhpfCWenv-wQLPnp2Th8MXn5udsiCRkgLMHNeJBrZNDxoITevJ7XPO9z42OJ4eGxEAjvdgwI5l8GjRtGloNYrK0MBwOJ5lvP2RPL-Kw7qDNJFcNzgPf7YrltOg8F2dhouhS5sCf8GQrEgKdx9HVjb2v7F3h-XGkJRr9WXUMqCQ",
        "refresh_token": "YzCAIPOXhalKNiPwK98Qo94jkDszotDLyY5refW-QR7JZHGHXRNn01QPjxKbiDIPA8TRWqR8oTTVbKcfw87Wy_Vz8i1Q8iFYUInLhkANIvJrJzfxCcLx_XI3G3oSJvp0",
        "scope": "openid",
        "id_token": "eyJraWQiOiI2YjdjN2UxYi0wYzdiLTQ2ODAtYWQ4OS1kZThkYzVkMGY5MDEiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ1c2VyMDAxIiwiYXVkIjoiYXBwMDEiLCJhenAiOiJhcHAwMSIsImF1dGhfdGltZSI6MTY5NTAxODg2NywiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwIiwiZXhwIjoxNjk1MDIxMTM2LCJpYXQiOjE2OTUwMTkzMzYsInNpZCI6Ill5WWVtcmtmRmRZNFdqOXEwNXNtM2tSMWR4V2trdE9VM1FucGp5MjZ3VWsifQ.XgFX4YaN0q_L1OijUr-bYhqPj2uaLGzREzzpbnnTUj-LavJhdPN85XZfqn7R1WUZGTpSzCiXhWmThFLZYoJBDTlU0qAMbO2jFmlsObVi6xplPLULZ-pAcavwCdt6YIIyyh39Id2Im_NE8lNbAxDYkbc5dl0yI3daCuXPNr3745Okr8qlhk6i1TdEQVJgH7GsE-0BxI8lphleZCtlvgUi0YrtzQEGpb4CIg6rOC9uFePXojeTOBAlCBBh5wRmdp7FaYX7TB_WGn0hm8OUixxsuKa0GYDbdxMZHh9M8MoZ_AuVibl3iwp9t33qRvUI8IwHNZ_SqWpCoIbfMLE034DHig",
        "token_type": "Bearer",
        "expires_in": 299
    }
    ```

6. Response tersebut bisa dilihat datanya di [https://jwt.io](https://jwt.io)

    ![[JWT.io](img/jwt-io.png)](img/jwt-io.png)


# Flow Grant Type Authorization Code dengan PKCE #

1. Generate dulu random string untuk menjadi  `code_verifier`, minimal 43 karakter, maksimal 128 karakter. Misalnya kita pakai `Molecule-Strum-Denatured-Daredevil-Shifty-Flyaway` sebagai `code_verifier`

2. Hitung `code_challenge` dengan rumus `code_challenge = base64urlencode(sha256(code_verifier))`. Untuk menghitungnya bisa menggunakan online tools misalnya [di link ini](https://tonyxu-io.github.io/pkce-generator/)

    * Code Verifier : 43 - 128 random string. Misal : `Molecule-Strum-Denatured-Daredevil-Shifty-Flyaway`
    * Code Challenge : base64urlencode(sha256(code verifier)). Misal : `dSEXRhA22A-fX2tJXmEIVkwntz54qa4dVIhBm--_BaU`

    Kode program untuk menghitung PKCE bisa dilihat pada [class PkceCalculator](src/main/java/com/muhardin/endy/training/microservices/authorization/server/helper/PkceCalculator.java).

3. URL Authorization Request : 

    [http://localhost:8080/oauth2/authorize?client_id=app02&response_type=code&redirect_uri=https://example.com/login/oauth2/code/catalog&scope=openid&state=1234&code_challenge_method=S256&code_challenge=dSEXRhA22A-fX2tJXmEIVkwntz54qa4dVIhBm--_BaU](http://localhost:8080/oauth2/authorize?client_id=app02&response_type=code&redirect_uri=https://example.com/login/oauth2/code/catalog&scope=openid&state=1234&code_challenge_method=S256&code_challenge=dSEXRhA22A-fX2tJXmEIVkwntz54qa4dVIhBm--_BaU)

4. Melakukan proses login, dan kemudian akan diredirect sesuai `redirect_uri` yang didaftarkan. Misalnya : `https://example.com/login/oauth2/code/catalog?code=Uz7_IUvI7IrRXNLh6bTQ5hNB2JkN6JZDjUAhU6vwiFoqfmYqjFWJa9aHF_1n-Atdw9OY82B5MPlYHlGWydXuk2Vem5XZaJabF_096K3wQ2_YYTEIfJjn_pSV0JAMtOOx&state=1234`

5. Lakukan HTTP request dengan menggunakan Postman atau Thunder Client. 

    [![HTTP Request ke token endpoint](img/public-client-token-request.png)](img/public-client-token-request.png)

    Public client tidak perlu mengirim `Basic Authentication`

    [![No Auth](img/public-client-no-auth.png)](img/public-client-no-auth.png)

    Berikut perintah CURLnya

    ```
    curl  -X POST \
    'http://localhost:8080/oauth2/token' \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --data-urlencode 'client_id=app02' \
    --data-urlencode 'redirect_uri=https://example.com/login/oauth2/code/catalog' \
    --data-urlencode 'grant_type=authorization_code' \
    --data-urlencode 'code=Uz7_IUvI7IrRXNLh6bTQ5hNB2JkN6JZDjUAhU6vwiFoqfmYqjFWJa9aHF_1n-Atdw9OY82B5MPlYHlGWydXuk2Vem5XZaJabF_096K3wQ2_YYTEIfJjn_pSV0JAMtOOx' \
    --data-urlencode 'code_verifier=Molecule-Strum-Denatured-Daredevil-Shifty-Flyaway'
    ```
  
6. Kita mendapatkan access token dan id token. Refresh token tidak diterbitkan karena tidak melakukan basic authentication

    [![Token Response](img/token-response.png)](img/token-response.png)