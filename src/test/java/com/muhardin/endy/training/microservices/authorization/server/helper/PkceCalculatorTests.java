package com.muhardin.endy.training.microservices.authorization.server.helper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PkceCalculatorTests {
    @Test
    public void testCalculatePkce() throws Exception {
        String verifier = "Molecule-Strum-Denatured-Daredevil-Shifty-Flyaway";
        String challenge = PkceCalculator.calculatePkce(verifier);
        System.out.println("Challenge : "+challenge);
        Assertions.assertEquals("dSEXRhA22A-fX2tJXmEIVkwntz54qa4dVIhBm--_BaU", challenge);
    }
}
