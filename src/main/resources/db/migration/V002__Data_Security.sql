-- password = abcd
insert into app_users (id, username, hashed_password, active) values 
('u001', 'user001', '{bcrypt}$2a$12$kJYjbJ3vi1KyFEP8V31GMeKa7lSZXMo7cClH2I0NtQwuutwPNJhSC', true);

insert into app_permissions (id, permission_label, permission_value) values 
('p001', 'View Transaksi', 'VIEW_TRANSAKSI');

insert into app_users_permissions (id_user, id_permission) values
('u001', 'p001');