create table app_users (
    id varchar(36),
    username varchar(100) not null,
    hashed_password varchar(255) not null,
    active boolean,
    primary key (id), 
    unique (username)
);

create table app_permissions (
    id varchar(36),
    permission_label varchar(100),
    permission_value varchar(100),
    primary key (id)
);

create table app_users_permissions (
    id_user varchar(36),
    id_permission varchar(36),
    primary key (id_user, id_permission)
);