package com.muhardin.endy.training.microservices.authorization.server.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;

@Configuration
public class SecurityConfig {
    private static final String SQL_LOGIN = """
            select username, hashed_password as password, active as enabled 
            from app_users where username = ?
            """;
    
    private static final String SQL_PERMISSION = """
            select u.username, p.permission_value as authority 
            from app_users u inner join app_users_permissions up on u.id = up.id_user 
            inner join app_permissions p on up.id_permission = p.id 
            where u.username = ?
            """;

    @Bean UserDetailsManager databaseLogin(DataSource dataSource){
        JdbcUserDetailsManager users = new JdbcUserDetailsManager(dataSource);
        users.setUsersByUsernameQuery(SQL_LOGIN);
        users.setAuthoritiesByUsernameQuery(SQL_PERMISSION);
        return users;
    }
}
