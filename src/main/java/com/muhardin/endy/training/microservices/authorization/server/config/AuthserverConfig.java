package com.muhardin.endy.training.microservices.authorization.server.config;

import java.util.UUID;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.oidc.OidcScopes;
import org.springframework.security.oauth2.server.authorization.JdbcOAuth2AuthorizationConsentService;
import org.springframework.security.oauth2.server.authorization.JdbcOAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationConsentService;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.client.JdbcRegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;

@Configuration
public class AuthserverConfig {

    @Bean
	public OAuth2AuthorizationService authorizationService(JdbcTemplate jdbcTemplate,
			RegisteredClientRepository registeredClientRepository) {
		return new JdbcOAuth2AuthorizationService(jdbcTemplate, registeredClientRepository);
	}

	@Bean
	public OAuth2AuthorizationConsentService authorizationConsentService(JdbcTemplate jdbcTemplate,
			RegisteredClientRepository registeredClientRepository) {
		// Will be used by the ConsentController
		return new JdbcOAuth2AuthorizationConsentService(jdbcTemplate, registeredClientRepository);
	}

    @Bean
    public RegisteredClientRepository registeredClientRepository(JdbcTemplate jdbcTemplate) {
		JdbcRegisteredClientRepository repository = new JdbcRegisteredClientRepository(jdbcTemplate);
        insertSamplePublicClient(repository);
        return repository;
	}

    private void insertSampleConfidentialClient(RegisteredClientRepository repository){
        RegisteredClient oidcClient = RegisteredClient.withId(UUID.randomUUID().toString())
				.clientId("app01")
                .clientName("Aplikasi 01")
                // client secret : abcd
				.clientSecret("{bcrypt}$2a$12$kJYjbJ3vi1KyFEP8V31GMeKa7lSZXMo7cClH2I0NtQwuutwPNJhSC")
				.clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
				.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
				.authorizationGrantType(AuthorizationGrantType.REFRESH_TOKEN)
				.redirectUri("http://client-app:20000/login/oauth2/code/catalog")
				.scope(OidcScopes.OPENID)
				.scope(OidcScopes.PROFILE)
				.clientSettings(ClientSettings.builder().requireAuthorizationConsent(true).build())
				.build();
        
        repository.save(oidcClient);
    }

	private void insertSamplePublicClient(RegisteredClientRepository repository){
        RegisteredClient oidcClient = RegisteredClient.withId(UUID.randomUUID().toString())
				.clientId("app02")
                .clientName("Aplikasi 02")
                // client secret : abcd
				//.clientSecret("{bcrypt}$2a$12$kJYjbJ3vi1KyFEP8V31GMeKa7lSZXMo7cClH2I0NtQwuutwPNJhSC")
				.clientAuthenticationMethod(ClientAuthenticationMethod.NONE)
				.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
				.authorizationGrantType(AuthorizationGrantType.REFRESH_TOKEN)
				.redirectUri("https://example.com/login/oauth2/code/catalog")
				.scope(OidcScopes.OPENID)
				.scope(OidcScopes.PROFILE)
				.clientSettings(ClientSettings.builder()
					.requireAuthorizationConsent(false)
					.requireProofKey(true) // harus melakukan flow PKCE
					.build())
				.build();
        
        repository.save(oidcClient);
    }
}
